require "application_system_test_case"

class TiketsTest < ApplicationSystemTestCase
  setup do
    @tiket = tikets(:one)
  end

  test "visiting the index" do
    visit tikets_url
    assert_selector "h1", text: "Tikets"
  end

  test "should create tiket" do
    visit tikets_url
    click_on "New tiket"

    fill_in "Descripcion", with: @tiket.descripcion
    fill_in "Fecha", with: @tiket.fecha
    fill_in "Monto", with: @tiket.monto
    click_on "Create Tiket"

    assert_text "Tiket was successfully created"
    click_on "Back"
  end

  test "should update Tiket" do
    visit tiket_url(@tiket)
    click_on "Edit this tiket", match: :first

    fill_in "Descripcion", with: @tiket.descripcion
    fill_in "Fecha", with: @tiket.fecha
    fill_in "Monto", with: @tiket.monto
    click_on "Update Tiket"

    assert_text "Tiket was successfully updated"
    click_on "Back"
  end

  test "should destroy Tiket" do
    visit tiket_url(@tiket)
    click_on "Destroy this tiket", match: :first

    assert_text "Tiket was successfully destroyed"
  end
end
