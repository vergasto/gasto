require "test_helper"

class TiketsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tiket = tikets(:one)
  end

  test "should get index" do
    get tikets_url
    assert_response :success
  end

  test "should get new" do
    get new_tiket_url
    assert_response :success
  end

  test "should create tiket" do
    assert_difference("Tiket.count") do
      post tikets_url, params: { tiket: { descripcion: @tiket.descripcion, fecha: @tiket.fecha, monto: @tiket.monto } }
    end

    assert_redirected_to tiket_url(Tiket.last)
  end

  test "should show tiket" do
    get tiket_url(@tiket)
    assert_response :success
  end

  test "should get edit" do
    get edit_tiket_url(@tiket)
    assert_response :success
  end

  test "should update tiket" do
    patch tiket_url(@tiket), params: { tiket: { descripcion: @tiket.descripcion, fecha: @tiket.fecha, monto: @tiket.monto } }
    assert_redirected_to tiket_url(@tiket)
  end

  test "should destroy tiket" do
    assert_difference("Tiket.count", -1) do
      delete tiket_url(@tiket)
    end

    assert_redirected_to tikets_url
  end
end
