class CreateTikets < ActiveRecord::Migration[7.1]
  def change
    create_table :tikets do |t|
      t.date :fecha
      t.integer :monto
      t.text :descripcion

      t.timestamps
    end
  end
end
