class ChangeColumnMontoToMontoCents < ActiveRecord::Migration[7.1]
  def self.up
    rename_column :tikets, :monto, :monto_cents
  end

  def self.down
    rename_column :tikets, :monto_cents, :monto
  end
end
