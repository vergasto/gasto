class AddTipoDePagoToTikets < ActiveRecord::Migration[7.1]
  def change
    add_reference :tikets, :tipo_de_pago, foreign_key: true
  end
end
