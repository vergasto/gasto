class CreateTipoDePagos < ActiveRecord::Migration[7.1]
  def change
    create_table :tipo_de_pagos do |t|
      t.string :metodo
      t.timestamps
    end
  end
end
