json.extract! tiket, :id, :fecha, :monto, :descripcion, :created_at, :updated_at
json.url tiket_url(tiket, format: :json)
