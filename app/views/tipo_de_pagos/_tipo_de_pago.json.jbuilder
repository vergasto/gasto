json.extract! tipo_de_pago, :id, :metodo, :has_many_tikets, :created_at, :updated_at
json.url tipo_de_pago_url(tipo_de_pago, format: :json)
