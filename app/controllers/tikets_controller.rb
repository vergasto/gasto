class TiketsController < ApplicationController
  before_action :set_tiket, only: %i[ show edit update destroy ]

  # GET /tikets or /tikets.json
  def index
    @tikets = Tiket.all
  end

  # GET /tikets/1 or /tikets/1.json
  def show
  end

  # GET /tikets/new
  def new
    @tiket = Tiket.new
  end

  # GET /tikets/1/edit
  def edit
  end

  # POST /tikets or /tikets.json
  def create
    @tiket = Tiket.new(tiket_params)

    respond_to do |format|
      if @tiket.save
        format.html { redirect_to tiket_url(@tiket), notice: "Tiket was successfully created." }
        format.json { render :show, status: :created, location: @tiket }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tiket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tikets/1 or /tikets/1.json
  def update
    respond_to do |format|
      if @tiket.update(tiket_params)
        format.html { redirect_to tiket_url(@tiket), notice: "Tiket was successfully updated." }
        format.json { render :show, status: :ok, location: @tiket }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tiket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tikets/1 or /tikets/1.json
  def destroy
    @tiket.destroy!

    respond_to do |format|
      format.html { redirect_to tikets_url, notice: "Tiket was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tiket
      @tiket = Tiket.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tiket_params
      params.require(:tiket).permit(:fecha, :monto, :monto_cents, :descripcion, :file, :tipo_de_pago_id)
    end
end
